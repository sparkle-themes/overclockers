<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Overclockers
 */

get_header();
?>

	<main id="primary" class="site-main">

		<section class="error-404 not-found container">
			<div class="page-header text-center">
				<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'overclockers' ); ?></h1>
			</div><!-- .page-header -->

			<div class="page-content text-center">
				<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'overclockers' ); ?></p>
			</div><!-- .page-content -->
		</section><!-- .error-404 -->

		<?php
		if( get_theme_mod('latest_blog_options', 'enable') == 'enable'): 
        get_template_part( "template-parts/home/latest-blog" );
    endif;

	?>

	</main><!-- #main -->

<?php
get_footer();
