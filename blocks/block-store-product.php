<?php

$buttonText = 'ADD TO BASKET';
$fileSaveLocation = '/../data/all-products.csv';

$id = block_value( 'product-id' );

if( ( $handle = fopen( __DIR__ . $fileSaveLocation, "r" ) ) !== FALSE )
{
	while( ( $data = fgetcsv( $handle, 0, "," ) ) !== FALSE )
	{
		if( $data[0] == $id )
		{
			PrintProductBlock(
				$data[4], // img
				// 'http://ocuk-v2.amplecube.co/wp-content/uploads/2023/07/product-img.png',
				ucwords( $data[6] ), // title
				$data[3], // description
				$data[5], // link
				$buttonText, 
				$data
			);

			break;
		}
	}
}