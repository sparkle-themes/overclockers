<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Overclockers
 */

get_header();
$userId = get_the_author_meta( 'ID' );
$name = get_the_author();
$avatar = get_avatar_url( $userId, [ 'size' => 300 ] );
$description = get_the_author_meta( 'description', $userId );
$twitter = get_the_author_meta( 'twitter', $userId );
$facebook = get_the_author_meta( 'facebook', $userId );
$instagram = get_the_author_meta( 'instagram', $userId );
$linkedin = get_the_author_meta( 'linkedin', $userId );
$youtube = get_the_author_meta( 'youtube', $userId );

$socials = '';
if( !empty( $facebook ) )
	$socials .= '<a href="' . $facebook . '" target="_blank" rel="noreferrer noopener"><i class="fab fa-facebook"></i></a>';
if( !empty( $twitter ) )
	$socials .= '<a href="' . $twitter . '" target="_blank" rel="noreferrer noopener"><i class="fab fa-twitter"></i></a>';
if( !empty( $instagram ) )
	$socials .= '<a href="' . $instagram . '" target="_blank" rel="noreferrer noopener"><i class="fab fa-instagram"></i></a>';
if( !empty( $linkedin ) )
	$socials .= '<a href="' . $linkedin . '" target="_blank" rel="noreferrer noopener"><i class="fab fa-linkedin"></i></a>';
if( !empty( $youtube ) )
	$socials .= '<a href="' . $youtube . '" target="_blank" rel="noreferrer noopener"><i class="fab fa-youtube"></i></a>';


?>
	<div class="latest-artical-section">
	<div id="primary" class="site-main container">
		
		<div id="featured-post" class="full-width author margin-top-small" data-author="<?php echo $userId; ?>">
			<img src="<?php echo $avatar; ?>" alt="<?php echo $name; ?>"/>
			<div class="featured-post-right">
				<div>
					<h2><?php echo $name; ?></h2>
					<p><?php echo $description; ?></p>
					<?php echo $socials; ?>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		
		
			
		<?php if ( have_posts() ) : ?>
		<h2 class="top-story newest-posts"><?php echo esc_html__( 'Newest posts', 'overclockers'); ?></h2>
		<div class="latest-artical">
			

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'grid' );

			endwhile;
			?>
			</div> <!-- latest-artical -->

			<?php

			// the_posts_navigation();
			overclockers_pagination();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
		

	</div><!-- #main -->
	</div>

<?php
// get_sidebar();
get_footer();
