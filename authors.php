<?php
/* Template Name: Authors */

global $wpdb;
get_header();
?>
<div class="latest-artical-section">
	<div id="primary" class="site-main container">
        <h2> <?php echo esc_html__( 'Our Editors', 'overclockers' ); ?> </h2>

        <div class="author-box-wrapper latest-artical">
            <?php

            $users = get_users();

            $sql = $wpdb->prepare( '
                    SELECT user_id
                    FROM wp_usermeta
                    WHERE meta_key = "%s"
            ', 'author_page' );
            $authors = $wpdb->get_results( $sql );

            foreach( $users as $user )
            {
                $authorPage = false;
                foreach( $authors as $author )
                {
                    if( $author->user_id == $user->ID )
                    {
                        $authorPage = true;
                        break;
                    }
                }

                if( !$authorPage )
                    continue;

                $meta_data = get_user_meta( $user->ID );
                    
                // print_r($meta_data);
                $url = get_author_posts_url( $user->ID );
                ?>
                <div class="news-block author-box">
                    <a href="<?php echo esc_url($url); ?>">
                        <img src='<?php echo get_avatar_url( $user->ID, [ 'size' => 500 ] ); ?>' alt="<?php echo $user->data->display_name; ?>" />
                    </a>
                    <h4><a href="<?php echo esc_url($url); ?>"><?php echo $user->data->display_name; ?></a></h4>
                    
                    <?php if( isset( $meta_data['shortbio'] ) ): ?>
                        <p><?php echo $meta_data['shortbio'][0]; ?></p>
                    <?php endif; ?>
                    <p>&nbsp;</p>
                    <a href="<?php  echo $url; ?>" class="btn btn-primary"> Find out more </a>

                </div>

                <?php
            }
            ?>
        </div>
    </div>
</div>
    
<?php
get_footer();