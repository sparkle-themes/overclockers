<?php
/**
 * Overclockers functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Overclockers
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function overclockers_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on Overclockers, use a find and replace
		* to change 'overclockers' to the name of your theme in all the template files.
		*/
	load_theme_textdomain( 'overclockers', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'slider-image', 322, 480, array( 'center', 'center' ) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__( 'Primary', 'overclockers' ),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'overclockers_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action( 'after_setup_theme', 'overclockers_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function overclockers_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'overclockers_content_width', 640 );
}
add_action( 'after_setup_theme', 'overclockers_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function overclockers_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'overclockers' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'overclockers' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'overclockers_widgets_init' );



if ( ! function_exists( 'overclockers_google_fonts_url' ) ) :
	/**
	 * Register Google fonts for Spark Multipurpose
	 *
	 * Create your own overclockers_google_fonts_url() function to override in a child theme.
	 *
	 * @since Spark Multipurpose 1.0.0
	 *
	 * @return string Google fonts URL for the theme.
	 */
    function overclockers_google_fonts_url() {
        $fonts_url = '';
        $font_families = $customizer_font_family = array();
		if ( 'off' !== _x( 'on', 'Prompt: on or off', 'overclockers' ) ) {
            $font_families[] = 'Prompt:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,600;1,700;1,800;1,900';
        }
	

        if( $font_families ) {
            $query_args = array(
                'family' => urlencode( implode( '|', $font_families ) ),
                'subset' => urlencode( 'latin,latin-ext' ),
            );
            $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
        }
		
		return esc_url( $fonts_url );
    }
endif;


add_filter('script_loader_tag', 'overclockers_add_type_attribute' , 10, 3);
function overclockers_add_type_attribute($tag, $handle, $src) {
    // if not your script, do nothing and return original $tag
    if ( 'overclockers-main' !== $handle ) {
        return $tag;
    }
    // change the script tag by adding type="module" and return it.
    $tag = '<script type="module" src="' . esc_url( $src ) . '"></script>';
    return $tag;
}

/**
 * Enqueue scripts and styles.
 */
function overclockers_scripts() {
	wp_enqueue_style( 'overclockers-fonts', overclockers_google_fonts_url(), array(), null );

	wp_enqueue_style( 'overclockers-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'overclockers-style', 'rtl', 'replace' );

	wp_enqueue_script( 'overclockers-main', get_template_directory_uri() . '/js/main.js', array('jquery'), _S_VERSION, true );

	wp_localize_script( 'overclockers-main', 'frontend_ajax_object',
		array( 
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
		)
	);

	wp_enqueue_script( 'overclockers-navigation', get_template_directory_uri() . '/js/navigation.js', array('jquery'), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'overclockers_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * custom controllers
 */
require get_template_directory() . '/inc/custom-controller/init.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';
require get_template_directory() . '/taxonomy-field.php';


/** 
 * Custom API
 */
require get_template_directory() . '/inc/api.php';

/**
 * Mobile Menu
 */
require get_template_directory() . '/inc/mobile-menu/init.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

function overclockers_post_category(){
	// List All Category
	$categories = get_categories();
	$blog_cat = array();
	foreach ($categories as $category) {
	    $blog_cat[$category->term_id] = $category->name;
	}

	return $blog_cat;
}


/**
 * Default color palettes
 *
 * @since Overclockers 1.0.0
 * @param null
 * @return array $overclockers_default_color_palettes
 *
 */
if ( ! function_exists( 'overclockers_default_color_palettes' ) ) {
	function overclockers_default_color_palettes() {
		$palettes = array(
			'#000000',
			'#ffffff',
			'#dd3333',
			'#dd9933',
			'#eeee22',
			'#81d742',
			'#1e73be',
			'#8224e3',
		);
		return apply_filters( 'overclockers_default_color_palettes', $palettes );
	}
}

function overclockers_category_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    }
    return $title;
}
add_filter( 'get_the_archive_title', 'overclockers_category_title' );

if ( ! function_exists( 'overclockers_breadcrumb' ) ) :

  /**
   * Breadcrumb.
   *
   * @since 1.0.0
   */
  function overclockers_breadcrumb() {

    if ( ! function_exists( 'breadcrumb_trail' ) ) {
      require_once trailingslashit( get_template_directory() ) . '/inc/breadcrumbs.php';
    }

    $breadcrumb_args = array(
      'container'   => 'div',
      'show_browse' => false,
    );

    breadcrumb_trail( $breadcrumb_args );

  }

endif;


if ( ! function_exists( 'overclockers_add_breadcrumb' ) ) :

  /**
   * Add breadcrumb.
   *
   * @since 1.0.0
   */
  function overclockers_add_breadcrumb() {

    ?>

    <div class="container breadcrumb-section">
       <div class="breadcrumb">
			<?php overclockers_breadcrumb(); ?>
       </div>
    </div>

  <?php

  }

endif;

add_action( 'overclockers_add_breadcrumb', 'overclockers_add_breadcrumb', 10 );
