<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Overclockers
 */

get_header();

global $paged;
if(get_query_var('paged')) {
	$paged = get_query_var('paged');
} else if(get_query_var('page')) {
	$paged = get_query_var('page');
} else {
	$paged = 1;
}

if( $paged == 1){
	get_template_part('template-parts/category/first', 'page' );
}else{
	get_template_part('template-parts/category/next', 'page' );
}


get_footer();
