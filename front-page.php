<?php get_header( );

/** home page sections */

    if( get_theme_mod('overclockers_slider_options', 'enable') == 'enable'): 
        get_template_part( "template-parts/home/slider" );
    endif;

    if( get_theme_mod('category1_options', 'enable') == 'enable'):
        
        $blog = get_theme_mod('category1_category');
        $cat_id = explode(',', $blog);
        $blog_posts = get_theme_mod('category1_post_count', 6);
        
        $args = array(
            'posts_per_page' => $blog_posts,
            'post_type' => 'post',
            'tax_query' => array(
                array(
                    'taxonomy' => 'category',
                    'field' => 'term_id',
                    'terms' => $cat_id
                ),
            ),
        );
        $blog_query = new WP_Query ($args);
        // print_r($blog_query->posts);

        $first = array_slice($blog_query->posts,0, 1); // apply array chunk
        $second = array_slice($blog_query->posts, 1, count($blog_query->posts));
        // print_r($first);
        echo "<div class='container'>";
        get_template_part( "template-parts/home/category", '', array('first' => $first, 'second' => $second, 'hr' => false) );
        echo "</div>";
    endif;

    if( get_theme_mod('popular_category_options', 'enable') == 'enable'): 
        get_template_part( "template-parts/home/popular-category" );
    endif;

    if( get_theme_mod('latest_blog_options', 'enable') == 'enable'): 
        get_template_part( "template-parts/home/latest-blog" );
    endif;


    if( get_theme_mod('news_category_options', 'enable') == 'enable'): 

        $categorys = get_theme_mod('home_single_category_sections');
        if(!empty( $categorys )){

            $categorys = json_decode( $categorys );
            $count = count($categorys);

            foreach($categorys as $index => $cat ){

                $cat_id = $cat->category;
                $blog_posts = $cat->postcount;
                $icon = $cat->icon;
                $args = array(
                    'posts_per_page' => $blog_posts,
                    'post_type' => 'post',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'category',
                            'field' => 'term_id',
                            'terms' => $cat_id
                        ),
                    ),
                );
                $blog_query = new WP_Query ($args);
                // print_r($blog_query->posts);

                $first = array_slice($blog_query->posts,0, 1); // apply array chunk
                $second = array_slice($blog_query->posts, 1, count($blog_query->posts));
                // print_r($first);
                echo "<div class='container'>";
                get_template_part( "template-parts/home/category", '', array(
                        'icon_image' => $icon, 
                        'cat_link' => $cat->cat_link, 
                        'cat_id' => $cat_id, 
                        'first' => $first, 
                        'second' => $second, 
                        'class' => $cat->class, 
                        'title' => $cat->title,
                        'hr' =>  ($count - 1) == $index ? false : true
                    ));
                echo "</div>";
            }
        }
        
    endif;

get_footer( );