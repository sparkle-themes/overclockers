<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Overclockers
 */
?>

<aside id="secondary" class="widget-area">
	<div class="author-profile">
		<?php 
			overclockers_posted_by_profile(); 

			$author_id = get_post_field('post_author' , $post->ID);
			
		?>

	</div>


	<div class="author-post">
		
		<h2><?php echo esc_html__('MORE POSTS BY', 'overclockers'); ?> <?php echo get_the_author_meta('display_name', $author_id);?></h2>
		<?php
			$args = array(
				'author'        =>  $author_id,
				'orderby'       =>  'post_date',
				'order'         =>  'ASC',
				'exclude'       => array(get_the_id()),
				'posts_per_page' => 5
			);
			$user_posts = get_posts( $args );
			if( $user_posts ): ?>
		<ul class="wp-block-latest-posts__list has-dates wp-block-latest-posts">
			<?php foreach( $user_posts as $p ): 
					// print_r($p);
				?>
			<li>
				<div class="wp-block-latest-posts__featured-image alignleft">
					<a href="<?php echo esc_url(get_the_permalink( $p) ); ?>">
						<?php echo get_the_post_thumbnail( $p ); ?>
					</a>
				</div>
				<a class="wp-block-latest-posts__post-title" href="<?php echo esc_url(get_the_permalink( $p) ); ?>"><?php echo $p->post_title; ?></a>
				<time class="wp-block-latest-posts__post-date"><?php echo get_the_date( 'M d, Y', $p );?></time>
			</li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>
	</div>


	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside><!-- #secondary -->
