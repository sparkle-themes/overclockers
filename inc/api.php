<?php
/**
 * Returns a date string
 * @param int $timestamp
 * @return false|string
 */
function GetDateString( $timestamp ) {
	$date = strftime( '%e. %b', $timestamp );
	$en = [
		'Jan',
		'Feb',
		'Mar',
		'Apr',
		'May',
		'Jun',
		'Jul',
		'Aug',
		'Sep',
		'Oct',
		'Nov',
		'Dec',
	];
	$de = [
		'Jan',
		'Feb',
		'März',
		'Apr',
		'Mai',
		'Juni',
		'Juli',
		'Aug',
		'Sept',
		'Okt',
		'Nov',
		'Dez',
	];
	$pt = [
		'jan',
		'fev',
		'março',
		'abril',
		'maio',
		'junho',
		'julho',
		'agosto',
		'set',
		'out',
		'nov',
		'dez',
	];

	if( language == 'de' )
		return str_ireplace( $en, $de, $date );
	else if( language == 'pt' )
		return str_ireplace( $en, $pt, $date );
	else
		return date( 'M j', $timestamp );
}

// Format posts
function whitelabel_get_post_array($posts){
	// Format posts
	$postAy = [];
	foreach( $posts as $post )
	{
		// Overclockers
		$url = get_permalink( $post );
		$img = get_the_post_thumbnail_url( $post->ID, [ 300, 0 ] );

		// Caseking
		$url = $url;
		$img = $img;

		
		$readTime = intval( sizeof( explode( " ", $post->post_content ) ) / 200 );
		if( $readTime < 1 )
			$readTime = 1;

		$title = substr( $post->post_title, 0, 94 );
		if( strlen( $post->post_title ) > 94 )
			$title .= '...';

		$postAy[] = [
			'url' => $url,
			'title' => $title,
			'author' => get_user_by( 'ID', $post->post_author )->display_name,
			'img' => $img,
			'date' => $post->post_date,
			'date-formatted' => GetDateString( strtotime( $post->post_date ) ),
			'read-time' => $readTime
		];
	}

	return $postAy;

}


add_action( 'rest_api_init', function() {
	register_rest_route( 'api', '/post-embed/', array(
		'methods' => 'GET',
		'callback' => 'GetPostEmbed',
		'permission_callback' => '__return_true'
	) );
} );

/**
 * Single post embed API endpoint
 * @param $data
 * @return WP_REST_Response
 */
function GetPostEmbed( $data ) {
	// Return error if no url is given
	if( empty( $data['url'] ) )
	{
		return new WP_REST_Response(
			[ 'error' => 'Missing url' ],
			200
		);
	}

	// Get the slug from the url (remove site address and slashes)
	$url = str_replace( root, '', $data['url'] );
	$url = str_replace( '/', '', $url );

	// Get post
	$args = array(
		'name' => $url,
		'post_type' => 'post',
		'post_status' => 'publish',
		'numberposts' => 1
	);
	$post = get_posts( $args );
	if( empty( $post ) )
	{
		return new WP_REST_Response(
			[ 'error' => 'Invalid url' ],
			200
		);
	}

	$url = $post[0]->guid;
	$img = get_the_post_thumbnail_url( $post[0]->ID, [ 300, 0 ] );

	$readTime = intval( sizeof( explode( " ", $post[0]->post_content ) ) / 200 );
	if( $readTime < 1 )
		$readTime = 1;

	// Return post data
	return new WP_REST_Response(
		[
			'url' => $post[0]->guid,
			'title' => $post[0]->post_title,
			'author' => get_user_by( 'ID', $post[0]->post_author )->display_name,
			'img' => get_the_post_thumbnail_url( $post[0]->ID, [ 300, 0 ] ),
			'date' => $post[0]->post_date,
			'date-formatted' => GetDateString( strtotime( $post[0]->post_date ) ),
			'read-time' => $readTime
		],
		200
	);
}



add_action( 'rest_api_init', function() {
	register_rest_route( 'api', '/latest-posts-embed/', array(
		'methods' => 'GET',
		'callback' => 'GetLatestPostsEmbed',
		'permission_callback' => '__return_true'
	) );
} );

/**
 * Latest posts embed API endpoint
 * @param $data
 * @return WP_REST_Response
 */
function GetLatestPostsEmbed( $data ) {
	// Set the amount of posts that will be returned
	$amount = 10;
	if( !empty( $data['amount'] ) )
	{
		$amount = filter_var( $data['amount'], FILTER_VALIDATE_INT );
		if( !is_int( $amount ) )
		{
			return new WP_REST_Response(
				[ 'error' => 'Invalid amount' ],
				200
			);
		}
	}

	// Get posts
	$args = array(
		'post_type' => 'post',
		'category_name' => $data['category'],
		'post_status' => 'publish',
		'numberposts' => $amount
	);
	$posts = get_posts( $args );
	if( empty( $posts ) )
	{
		return new WP_REST_Response(
			[ 'error' => 'No posts under the category: ' . $data['category'] ],
			200
		);
	}

	// Format posts
	$postAy = whitelabel_get_post_array($posts);

	// Return posts data
	return new WP_REST_Response(
		[ 'posts' => $postAy ],
		200
	);
}



add_action( 'rest_api_init', function(){
	register_rest_route( 'api', '/tag-posts-embed/', array(
		'methods' => 'GET',
		'callback' => 'GetTagPostsEmbed',
		'permission_callback' => '__return_true'
	) );
} );
/**
 * Tag post embed API endpoint
 * @param $data
 * @return WP_REST_Response
 */
function GetTagPostsEmbed( $data ) {
	// Set the amount of posts that will be returned
	$amount = 10;
	if( !empty( $data['amount'] ) )
	{
		$amount = filter_var( $data['amount'], FILTER_VALIDATE_INT );
		if( !is_int( $amount ) )
		{
			return new WP_REST_Response(
				[ 'error' => 'Invalid amount' ],
				200
			);
		}
	}

	// Return error if no url is given
	if( empty( $data['tag'] ) ) {
		return new WP_REST_Response(
			[ 'error' => 'Missing Tag' ],
			200
		);
	}

	$terms = explode( ',',  $data['tag'] );

	// Get post
	$args = array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'posts_per_page' => $amount,
		'tax_query' => array(
            array(
                'taxonomy'  => 'post_tag',
                'field'     => 'slug',
                'terms'     => $terms
            )
		)
		
	);

	$posts = get_posts( $args );
	if( empty( $posts ) )
	{
		return new WP_REST_Response(
			[ 'error' => 'No posts under the tag: ' . $data['tag'] ],
			200
		);
	}

	$postAy = whitelabel_get_post_array($posts);

	// Return posts data
	return new WP_REST_Response(
		[ 'posts' => $postAy ],
		200
	);
}



add_action( 'rest_api_init', function(){
	register_rest_route( 'api', '/pid-posts-embed/', array(
		'methods' => 'GET',
		'callback' => 'GetPidPostsEmbed',
		'permission_callback' => '__return_true'
	) );
} );

/**
 * Tag post embed API endpoint
 * @param $data
 * @return WP_REST_Response
 */
function GetPidPostsEmbed( $data ){
	// Return error if no url is given
	if( empty( $data['id'] ) ) {
		return new WP_REST_Response(
			[ 'error' => 'Missing id' ],
			200
		);
	}


	// Get post
	$args = array(
		'post_type' => 'post',
		'post_status' => 'publish',
		
	);

	if( isset( $data['id'] ) ){
		// Get the slug from the url (remove site address and slashes)
		$ids = str_replace( root, '', $data['id'] );
		$args['post__in'] = explode( ',', $ids );
	}

	$posts = get_posts( $args );
	if( empty( $posts ) )
	{
		return new WP_REST_Response(
			[ 'error' => 'No posts under the ids: '. $data['id'] ],
			200
		);
	}

	$postAy = whitelabel_get_post_array($posts);

	// Return posts data
	return new WP_REST_Response(
		[ 'posts' => $postAy ],
		200
	);
}