<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Overclockers
 */

if( !function_exists('overclockers_posted_time_ago')):
	function overclockers_posted_time_ago() {
		return human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ).' '.__( 'ago' );
	}
endif;

//estimated reading time
function overclockers_reading_time( $post ) {
	$content = get_post_field( 'post_content', $post->ID );
	$word_count = str_word_count( strip_tags( $content ) );
	$readingtime = ceil($word_count / 200);

	if ($readingtime == 1) {
		$timer = " min read";
	} else {
		$timer = " min read";
	}

	$totalreadingtime = $readingtime . $timer;

	return $totalreadingtime;
}

if ( ! function_exists( 'overclockers_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function overclockers_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( '%s', 'post date', 'overclockers' ),
			'<a href="javascript:void(0)" rel="bookmark">' . $time_string . '</a>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

	}
endif;

if ( ! function_exists( 'overclockers_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function overclockers_posted_by($post = null) {
		if( $post ){
			setup_postdata($post);
			// global $post;
			// $post = $post;
		}

		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( '%s', 'post author', 'overclockers' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="byline"> ' . $byline . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

	}
endif;

if ( ! function_exists( 'overclockers_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function overclockers_entry_footer() {
		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'overclockers' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				)
			);
			echo '</span>';
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'overclockers' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'overclockers_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function overclockers_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<div class="post-thumbnail">
				<?php the_post_thumbnail(); ?>
			</div><!-- .post-thumbnail -->

		<?php else : ?>

			<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
				<?php
					the_post_thumbnail(
						'post-thumbnail',
						array(
							'alt' => the_title_attribute(
								array(
									'echo' => false,
								)
							),
						)
					);
				?>
			</a>

			<?php
		endif; // End is_singular().
	}
endif;

if ( ! function_exists( 'wp_body_open' ) ) :
	/**
	 * Shim for sites older than 5.2.
	 *
	 * @link https://core.trac.wordpress.org/ticket/12563
	 */
	function wp_body_open() {
		do_action( 'wp_body_open' );
	}
endif;


/**
 * Numbered Pagination
 *
 * @since	1.0.0
 * @link	https://codex.wordpress.org/Function_Reference/paginate_links
 */
function overclockers_pagination($query = '', $current_page = null, $echo = true) {

	// Arrows with RTL support
	$prev_arrow = is_rtl() ? 'fa fa-angle-right' : 'fa fa-angle-left';
	$next_arrow = is_rtl() ? 'fa fa-angle-left' : 'fa fa-angle-right';

	// Get global $query
	if (!$query) {
		global $wp_query;
		$query = $wp_query;
	}

	// Set vars
	$total = $query->max_num_pages;
	$big = 999999999;

	// Display pagination if total var is greater then 1 (current query is paginated)
	if ($total > 1) {

		if( $current_page == null){
			// Get current page
			if ($current_page = get_query_var('paged')) {
				$current_page = $current_page;
			} elseif ($current_page = get_query_var('page')) {
				$current_page = $current_page;
			} else {
				$current_page = 1;
			}
		}

		// Get permalink structure
		if (get_option('permalink_structure')) {
			if (is_page()) {
				$format = 'page/%#%/';
			} else {
				$format = '/%#%/';
			}
		} else {
			$format = '&paged=%#%';
		}

		$args = apply_filters('overclockers_pagination_args', array(
			'base' => str_replace($big, '%#%', html_entity_decode(get_pagenum_link($big))),
			'format' => $format,
			'current' => max(1, $current_page),
			'total' => $total,
			'show_all' => false,
			'mid_size' => 3,
			'type' => 'list',
			'prev_next'          => true,
			'prev_text' => '<span class="scr-only">&#x3c;</span>',
			'next_text' => '<span class="scr-only">&#x3e;</span>',
			'base_module' => 10
		));

		echo '<div class="page-navigation">';
		echo '<ul class="pagination">';
			$args2 = $args;
			$args2['type'] = "array";
			$args2['show_all'] = true;
			$arra = ocuk_paginate_links2($args2);
			
			foreach($arra as $key => $val){
				echo "<li class='page-item'>". $val. "</li>";
			}
		echo "</ul>";
		echo "</div>";

		// // Output pagination
		// if ($echo) {
		// 	echo '<div class="bew-pagination clr">' . wp_kses_post(paginate_links($args)) . '</div>';
		// } else {
		// 	return '<div class="bew-pagination clr">' . wp_kses_post(paginate_links($args)) . '</div>';
		// }
	}
}

function ocuk_paginate_links2( $args = '' ) {
	global $wp_query, $wp_rewrite;

	// Setting up default values based on the current URL.
	$pagenum_link = html_entity_decode( get_pagenum_link() );
	$url_parts    = explode( '?', $pagenum_link );

	// Get max pages and current page out of the current query, if available.
	$total   = isset( $wp_query->max_num_pages ) ? $wp_query->max_num_pages : 1;
	$current = get_query_var( 'paged' ) ? (int) get_query_var( 'paged' ) : 1;

	// Append the format placeholder to the base URL.
	$pagenum_link = trailingslashit( $url_parts[0] ) . '%_%';

	// URL base depends on permalink settings.
	$format  = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';

	$defaults = array(
		'base'               => $pagenum_link, // http://example.com/all_posts.php%_% : %_% is replaced by format (below).
		'format'             => $format, // ?page=%#% : %#% is replaced by the page number.
		'total'              => $total,
		'current'            => $current,
		'aria_current'       => 'page',
		'show_all'           => false,
		'prev_next'          => true,
		'prev_text'          => __( '&laquo; Previous' ),
		'next_text'          => __( 'Next &raquo;' ),
		'end_size'           => 1,
		'mid_size'           => 2,
		'type'               => 'plain',
		'add_args'           => array(), // Array of query args to add.
		'add_fragment'       => '',
		'before_page_number' => '',
		'after_page_number'  => '',
	);

	$args = wp_parse_args( $args, $defaults );

	if ( ! is_array( $args['add_args'] ) ) {
		$args['add_args'] = array();
	}

	// Merge additional query vars found in the original URL into 'add_args' array.
	if ( isset( $url_parts[1] ) ) {
		// Find the format argument.
		$format       = explode( '?', str_replace( '%_%', $args['format'], $args['base'] ) );
		$format_query = isset( $format[1] ) ? $format[1] : '';
		wp_parse_str( $format_query, $format_args );

		// Find the query args of the requested URL.
		wp_parse_str( $url_parts[1], $url_query_args );

		// Remove the format argument from the array of query arguments, to avoid overwriting custom format.
		foreach ( $format_args as $format_arg => $format_arg_value ) {
			unset( $url_query_args[ $format_arg ] );
		}

		$args['add_args'] = array_merge( $args['add_args'], urlencode_deep( $url_query_args ) );
	}

	// Who knows what else people pass in $args.
	$total = (int) $args['total'];
	if ( $total < 2 ) {
		return;
	}
	$current  = (int) $args['current'];
	$end_size = (int) $args['end_size']; // Out of bounds? Make it the default.
	if ( $end_size < 1 ) {
		$end_size = 1;
	}
	$mid_size = (int) $args['mid_size'];
	if ( $mid_size < 0 ) {
		$mid_size = 2;
	}

	$add_args   = $args['add_args'];
	$r          = '';
	$page_links = array();
	$dots       = false;

	if ( $args['prev_next'] && $current && 1 < $current ) :
		$link = str_replace( '%_%', 2 == $current ? '' : $args['format'], $args['base'] );
		$link = str_replace( '%#%', $current - 1, $link );
		if ( $add_args ) {
			$link = add_query_arg( $add_args, $link );
		}
		$link .= $args['add_fragment'];

		$page_links[] = sprintf(
			'<a class="prev page-numbers" href="%s">%s</a>',
			/**
			 * Filters the paginated links for the given archive pages.
			 *
			 * @since 3.0.0
			 *
			 * @param string $link The paginated link URL.
			 */
			esc_url( apply_filters( 'paginate_links', $link ) ),
			$args['prev_text']
		);
	endif;

	$base_module = 10;
	$allowd_prev_number = $current - $mid_size;
	$allowd_next_number = $current + $mid_size;

	$allowd_next_base_number = $base_module * ( $mid_size ) + $current;
	
	$allowd_prev_base_end_number = $current - $base_module ;
	$allowd_prev_base_start_number = $current - ( $base_module * ( $mid_size  ) );

	for ( $n = 1; $n <= $total; $n++ ) :
		if ( $n == $current ) :
			$page_links[] = sprintf(
				'<span aria-current="%s" class="page-numbers current">%s</span>',
				esc_attr( $args['aria_current'] ),
				$args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number']
			);

			$dots = true;
		else :
			if ( $args['show_all'] || ( $n <= $end_size || ( $current && $n >= $current - $mid_size && $n <= $current + $mid_size ) || $n > $total - $end_size ) ) :
				$link = str_replace( '%_%', 1 == $n ? '' : $args['format'], $args['base'] );
				$link = str_replace( '%#%', $n, $link );
				if ( $add_args ) {
					$link = add_query_arg( $add_args, $link );
				}
				$link .= $args['add_fragment'];
				
				if( $n == 1){
					$link = str_replace( '/page/1/', '/', $link );
				}
				
				//  
				if( 
					( $n > $allowd_prev_number && $n < $current )
					|| ( $n > $current && $n < $allowd_next_number )  
					
					|| ( $n > $allowd_prev_number && $n % $base_module == 0 && $n <= $allowd_next_base_number ) // next base multiplayer
					|| ( $n <= $allowd_prev_base_end_number && $n % $base_module == 0 && $n >= $allowd_prev_base_start_number ) // prev base multiplayer

					|| $n == 1  // first pge
					|| $n > $total - $end_size // last page
					|| ( $current < 4 && $n <= 5 )
					) {
					$page_links[] = sprintf(
						'<a class="page-numbers" href="%s">%s</a>',
						/** This filter is documented in wp-includes/general-template.php */
						esc_url( apply_filters( 'paginate_links', $link ) ),
						$args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number']
					);
				}elseif ( $dots && ( $n < $current && $n > $allowd_prev_number - 1 ) || ( $n > $current && $n < $allowd_next_number + 1 ) || ( $current < 10 && $n == 6 ) ) {
					$page_links[] = '<span class="page-numbers dots">' . __( '&hellip;' ) . '</span>';
				}
				


				

				$dots = true;
			elseif ( $dots && ! $args['show_all'] ) :
				$page_links[] = '<span class="page-numbers dots">' . __( '&hellip;' ) . '</span>';

				$dots = false;
			endif;
		endif;
	endfor;

	if ( $args['prev_next'] && $current && $current < $total ) :
		$link = str_replace( '%_%', $args['format'], $args['base'] );
		$link = str_replace( '%#%', $current + 1, $link );
		if ( $add_args ) {
			$link = add_query_arg( $add_args, $link );
		}
		$link .= $args['add_fragment'];

		$page_links[] = sprintf(
			'<a class="next page-numbers" href="%s">%s</a>',
			/** This filter is documented in wp-includes/general-template.php */
			esc_url( apply_filters( 'paginate_links', $link ) ),
			$args['next_text']
		);
	endif;

	switch ( $args['type'] ) {
		case 'array':
			return $page_links;

		case 'list':
			$r .= "<ul class='page-numbers'>\n\t<li>";
			$r .= implode( "</li>\n\t<li>", $page_links );
			$r .= "</li>\n</ul>\n";
			break;

		default:
			$r = implode( "\n", $page_links );
			break;
	}

	/**
	 * Filters the HTML output of paginated links for archives.
	 *
	 * @since 5.7.0
	 *
	 * @param string $r    HTML output.
	 * @param array  $args An array of arguments. See paginate_links()
	 *                     for information on accepted arguments.
	 */
	$r = apply_filters( 'paginate_links_output', $r, $args );

	return $r;
}


if( !function_exists('overclockers_posted_by_profile')):
	function overclockers_posted_by_profile(){
		global $post;
 
		// Get the author ID    
		$author_id = get_post_field('post_author' , $post->ID);
		
		// Get the author image URL    
		$output = get_avatar_url($author_id);
		$authorDesc = get_the_author_meta('description', $author_id); 
		$twitter = get_the_author_meta( 'twitter' );
		?>
		<div class="author-profile-image">
			<div class="profile-img">
				<a href="<?php echo esc_url( get_author_posts_url( $author_id ) ); ?>">
					<?php echo get_avatar($author_id, 100); ?>
				</a>
			</div>
			<div class="profil-name"> 
				<div class="dec-wrapper">
					<div class="profile-wrapper">	
						<h3><span><a href="<?php echo esc_url( get_author_posts_url( $author_id ) ); ?>"><?php echo get_the_author_meta('display_name', $author_id);?></a></span></h3>
						<p>Author</p>
					</div>
					<?php if( $twitter ): ?>
					<div class="twitter">
						<a href="<?php echo esc_url($twitter); ?>"><img src="<?php echo get_template_directory_uri(  ); ?>/images/twitterbtn.png"></a>
					</div>
					<?php endif; ?>
				</div>
				<div class="aurther-description">
					<p>
						<?php echo $authorDesc; ?>
						<a style="text-decoration: underline; font-weight:bold;" href="<?php echo esc_url( get_author_posts_url( $author_id ) ); ?>"><br/><?php echo esc_html__( 'Read More >>', 'caseking' ); ?></a>
					<p>
				</div>
			</div>
			
		</div>
		<?php
	}
endif;

if( !function_exists('overclockers_share_section')):
	function overclockers_share_section(){
		
		global $post;
		?>

		<div class="section-share">
			<?php echo do_shortcode( '[SSB]' ); ?>
			<ul>
				<!-- <li class="facebook"><a href="#"><img src="<?php echo get_template_directory_uri(  ); ?>/images/fbicon.png"> 120</a></li>
				<li class="twitter"><a href="#"><img src="<?php echo get_template_directory_uri(  ); ?>/images/twitterbtn.png">100</a></li>
				<li class="link"><a href="#"><img src="<?php echo get_template_directory_uri(  ); ?>/images/shareicon.png">250</a></li> -->
				<li class="pst-time"><?php echo overclockers_reading_time($post); ?>  </li>
			</ul>
		</div>

		<?php 
		echo '';
	}
endif;

if( !function_exists('overclockers_colorized_category')){
	function overclockers_colorized_category(){
		global $post;
		$categories = get_the_category();
		if ( ! empty( $categories ) ) {
			foreach($categories as $category){
				$color = get_term_meta ( $category -> term_id, 'category-color-id', true );
				$style = "";
				if( $color ){
					$style = "style='background:{$color};'";
				}
				if( strtolower( $category->name) != 'featured small' &&  strtolower( $category->name) != 'featured big' ){ 
					echo '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '"'. $style .'>' . esc_html( $category->name ) . '</a>';
				}
			}
		}
	}
}