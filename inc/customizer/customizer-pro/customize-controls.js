(function (api) {

	// Extends our custom "overclockers" section.
	api.sectionConstructor[ 'overclockers' ] = api.Section.extend({

		// No events for this type of section.
		attachEvents: function () { },

		// Always make the section active.
		isContextuallyActive: function () {
			return true;
		}
	});

})(wp.customize);



// Extends our custom section.
(function (api) {

	api.sectionConstructor[ 'overclockers-upgrade-section' ] = api.Section.extend({

		// No events for this type of section.
		attachEvents: function () { },

		// Always make the section active.
		isContextuallyActive: function () {
			return true;
		}
	});

})(wp.customize);
