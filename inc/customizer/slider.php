<?php        
/**
 *	Main Banner Slider.
*/
$wp_customize->add_section(new Overclockers_Toggle_Section($wp_customize, 'overclockers_banner_slider', array(
    'title'		=>	esc_html__('Home Slider Settings','overclockers'),
    'panel'		=> 'overclockers_homepage_settings',
    'priority'  => -1,
    'hiding_control' => 'overclockers_slider_options'
)));

/**
 * Banner Slider
*/
    $wp_customize->add_setting( 'overclockers_slider_options', array(
        'default'   =>  'enable',
        'transport' => 'postMessage',
        'sanitize_callback'  =>  'sanitize_text_field',
    ));

    $wp_customize->add_control(new Overclockers_Switch_Control( $wp_customize,'overclockers_slider_options', 
        array(
            'section'       => 'overclockers_banner_slider',
            'label'         =>  esc_html__('Enable Slider', 'overclockers'),
            'type'          =>  'switch',
            'switch_label' => array(
                'enable' => esc_html__('Yes', 'overclockers'),
                'disable' => esc_html__('No', 'overclockers'),
            ),
        )
    ));

    
    // Blog Posts.
    $wp_customize->add_setting('slider_category', array(
        'transport' => 'postMessage',
        'sanitize_callback' => 'sanitize_text_field',     //done
    ));
    $wp_customize->add_control(new Overclockers_Multiple_Check_Control($wp_customize, 'slider_category', array(
        'label'    => esc_html__('Select Category', 'spark-multipurpose'),
        'settings' => 'slider_category',
        'section'  => 'overclockers_banner_slider',
        'choices'  => overclockers_post_category(),
    )));



    $wp_customize->add_setting('slider_count', array(
        'transport' => 'postMessage',
        'sanitize_callback' => 'sanitize_text_field',     //done
    ));
    $wp_customize->add_control(new Overclockers_Range_Control($wp_customize, 'slider_count', array(
        'label'    => esc_html__('Number of posts', 'spark-multipurpose'),
        'settings' => 'slider_count',
        'default' => 6,
        'input_attrs' => array(
            'min' => 3,
            'max' => 100,
            'step' => 1
        ),
        
        'section'  => 'overclockers_banner_slider'
    )));


    
    $wp_customize->selective_refresh->add_partial('overclockers_slider_selective_refresh', array(
        'settings' => array('overclockers_slider_options','slider_category', 'slider_count'),
        'selector' => '.slide-container',
        'container_inclusive' => true,
        'render_callback' => function() {
            if( in_array( get_theme_mod('overclockers_slider_options', 'enable') , array( 1, 'enable')) ) {
                return overclockers_banner_section();
            }
        }
    ));