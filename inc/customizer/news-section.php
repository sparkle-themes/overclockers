<?php        
/**
 *	Main Banner Slider.
*/
$wp_customize->add_section(new Overclockers_Toggle_Section($wp_customize, 'news_category_section', array(
    'title'		=>	esc_html__('News Section','overclockers'),
    'panel'		=> 'overclockers_homepage_settings',
    'priority'  => -1,
    'hiding_control' => 'news_category_options'
)));


    $wp_customize->add_setting( 'news_category_options', array(
        'default'   =>  'enable',
        // 'transport' => 'postMessage',
        'sanitize_callback'  =>  'sanitize_text_field',
    ));

    $wp_customize->add_control(new Overclockers_Switch_Control( $wp_customize,'news_category_options', 
        array(
            'section'       => 'news_category_section',
            'label'         =>  esc_html__('Enable', 'overclockers'),
            'type'          =>  'switch',
            'switch_label' => array(
                'enable' => esc_html__('Yes', 'overclockers'),
                'disable' => esc_html__('No', 'overclockers'),
            ),
        )
    ));


    $wp_customize->add_setting('home_single_category_sections', array(
        // 'transport' => 'postMessage',
        'sanitize_callback' => 'overclockers_sanitize_repeater',		//done
        'default' => json_encode(array(
            array(
                
            )
        ))
    ));
    $wp_customize->add_control(new Overclockers_Repeater_Control( $wp_customize, 
        'home_single_category_sections', 
        array(
            'label' 	   => esc_html__('Category News', 'overclockers'),
            'section'       => 'news_category_section',
            'settings' 	   => 'home_single_category_sections',
            'box_label' => esc_html__('Category', 'overclockers'),
            'add_label' => esc_html__('Add New', 'overclockers'),
        ),
        array(
            'icon' => array(
                'type' => 'upload',
                'label' => esc_html__('Icon', 'overclockers'),
                'default' => ''
            ),
            'title' => array(
                'type' => 'text',
                'label' => esc_html__('Title', 'overclockers'),
                'default' => ''
            ),
            'category' => array(
                'type' => 'select',
                'label' => esc_html__('Category', 'overclockers'),
                'options' => overclockers_post_category()
            ),

            'postcount' => array(
                'type' => 'number',
                'default' => 5,
                'label' => esc_html__( "No of post", 'overclockers' )
            ),
            'cat_link' => array(
                'type' => 'checkbox',
                'label' => esc_html__('Show Link', 'overclockers'),
                'default' => false
            ),

            // 'pagination' => array(
            //     'type' => 'checkbox',
            //     'label' => esc_html__('Pagination', 'overclockers'),
            //     'default' => false
            // ),

            'class' => array(
                'type' => 'text',
                'label' => esc_html__('Extra Class', 'overclockers'),
                'default' => 'news-section'
            ),
        )
    ));