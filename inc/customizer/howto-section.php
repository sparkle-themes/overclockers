<?php        
/**
 *	Main Banner Slider.
*/
$wp_customize->add_section(new Overclockers_Toggle_Section($wp_customize, 'howto_category_section', array(
    'title'		=>	esc_html__('How To Section','overclockers'),
    'panel'		=> 'overclockers_homepage_settings',
    'priority'  => -1,
    'hiding_control' => 'howto_category_options'
)));

/**
 * Banner Slider
*/
    $wp_customize->add_setting( 'howto_category_options', array(
        'default'   =>  'enable',
        'transport' => 'postMessage',
        'sanitize_callback'  =>  'sanitize_text_field',
    ));

    $wp_customize->add_control(new Overclockers_Switch_Control( $wp_customize,'howto_category_options', 
        array(
            'section'       => 'howto_category_section',
            'label'         =>  esc_html__('Enable', 'overclockers'),
            'type'          =>  'switch',
            'switch_label' => array(
                'enable' => esc_html__('Yes', 'overclockers'),
                'disable' => esc_html__('No', 'overclockers'),
            ),
        )
    ));


    $wp_customize->add_setting( 'howto_category_icon', array(
        'sanitize_callback' => 'sanitize_text_field', 	 //done	
        'transport' => 'postMessage'
    ));

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'howto_category_icon', array(
        'label'	   => esc_html__('Icon Image','spark-multipurpose'),
        'section'  => 'howto_category_section',
    )));
    
    // Blog Posts.
    $wp_customize->add_setting('howto_category_category', array(
        'transport' => 'postMessage',
        'sanitize_callback' => 'sanitize_text_field',     //done
    ));
    $wp_customize->add_control( 'howto_category_category', array(
        'type' => 'select',
        'label'    => esc_html__('Select Category', 'spark-multipurpose'),
        'settings' => 'howto_category_category',
        'section'  => 'howto_category_section',
        'choices'  => overclockers_post_category(),
    ));

    $wp_customize->add_setting('howto_section_post_count', array(
        'transport' => 'postMessage',
        'sanitize_callback' => 'sanitize_text_field',     //done
    ));
    $wp_customize->add_control(new Overclockers_Range_Control($wp_customize, 'howto_section_post_count', array(
        'label'    => esc_html__('Number of posts', 'spark-multipurpose'),
        'settings' => 'howto_section_post_count',
        'default' => 6,
        'input_attrs' => array(
            'min' => 3,
            'max' => 100,
            'step' => 1
        ),
        
        'section'  => 'howto_category_section'
    )));




    $wp_customize->selective_refresh->add_partial('howto_category_section', array(
        'settings' => array('howto_category_options','howto_category_options'),
        'selector' => '.ed-slider',
        'container_inclusive' => true,
        'render_callback' => function() {
            if( in_array( get_theme_mod('howto_category_options', 'enable') , array( 1, 'enable')) ) {
                return overclockers_banner_section();
            }
        }
    ));