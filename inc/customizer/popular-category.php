<?php        
/**
 *	Main Banner Slider.
*/
$wp_customize->add_section(new Overclockers_Toggle_Section($wp_customize, 'popular_category_section', array(
    'title'		=>	esc_html__('Popular Category','overclockers'),
    'panel'		=> 'overclockers_homepage_settings',
    'priority'  => -1,
    'hiding_control' => 'popular_category_options'
)));

/**
 * Banner Slider
*/
    $wp_customize->add_setting( 'popular_category_options', array(
        'default'   =>  'enable',
        'transport' => 'postMessage',
        'sanitize_callback'  =>  'sanitize_text_field',
    ));

    $wp_customize->add_control(new Overclockers_Switch_Control( $wp_customize,'popular_category_options', 
        array(
            'section'       => 'popular_category_section',
            'label'         =>  esc_html__('Enable', 'overclockers'),
            'type'          =>  'switch',
            'switch_label' => array(
                'enable' => esc_html__('Yes', 'overclockers'),
                'disable' => esc_html__('No', 'overclockers'),
            ),
        )
    ));

    
    // Team Section Title.
    $wp_customize->add_setting( 'popular_category_title', array(
        'transport' => 'postMessage',
        'sanitize_callback' => 'sanitize_text_field'			//done
    ) );
    $wp_customize->add_control( 'popular_category_title', array(
        'label'    => esc_html__( 'Title', 'overclockers' ),
        'section'  => 'popular_category_section',
        'type'     => 'text',
    ));

    // Blog Posts.
    $wp_customize->add_setting('pupular_category_category', array(
        'transport' => 'postMessage',
        'sanitize_callback' => 'sanitize_text_field',     //done
    ));
    $wp_customize->add_control(new Overclockers_Multiple_Check_Control($wp_customize, 'pupular_category_category', array(
        'label'    => esc_html__('Select Category', 'spark-multipurpose'),
        'settings' => 'pupular_category_category',
        'section'  => 'popular_category_section',
        'choices'  => overclockers_post_category(),
    )));



    $wp_customize->selective_refresh->add_partial('popular_category_section', array(
        'settings' => array('popular_category_options','popular_category_options'),
        'selector' => '.ed-slider',
        'container_inclusive' => true,
        'render_callback' => function() {
            if( in_array( get_theme_mod('popular_category_options', 'enable') , array( 1, 'enable')) ) {
                return overclockers_banner_section();
            }
        }
    ));