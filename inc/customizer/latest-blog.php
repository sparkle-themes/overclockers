<?php        
/**
 *	Main Banner Slider.
*/
$wp_customize->add_section(new Overclockers_Toggle_Section($wp_customize, 'latest_blog_section', array(
    'title'		=>	esc_html__('Latest Blog','overclockers'),
    'panel'		=> 'overclockers_homepage_settings',
    'priority'  => -1,
    'hiding_control' => 'latest_blog_options'
)));

/**
 * Banner Slider
*/
    $wp_customize->add_setting( 'latest_blog_options', array(
        'default'   =>  'enable',
        'transport' => 'postMessage',
        'sanitize_callback'  =>  'sanitize_text_field',
    ));

    $wp_customize->add_control(new Overclockers_Switch_Control( $wp_customize,'latest_blog_options', 
        array(
            'section'       => 'latest_blog_section',
            'label'         =>  esc_html__('Enable', 'overclockers'),
            'type'          =>  'switch',
            'switch_label' => array(
                'enable' => esc_html__('Yes', 'overclockers'),
                'disable' => esc_html__('No', 'overclockers'),
            ),
        )
    ));

    
    // Team Section Title.
    $wp_customize->add_setting( 'latest_blog_title', array(
        'transport' => 'postMessage',
        'sanitize_callback' => 'sanitize_text_field'			//done
    ) );
    $wp_customize->add_control( 'latest_blog_title', array(
        'label'    => esc_html__( 'Title', 'overclockers' ),
        'section'  => 'latest_blog_section',
        'type'     => 'text',
    ));

    // Blog Posts.
    $wp_customize->add_setting('latest_blog_category', array(
        'transport' => 'postMessage',
        'sanitize_callback' => 'sanitize_text_field',     //done
    ));
    $wp_customize->add_control(new Overclockers_Multiple_Check_Control($wp_customize, 'latest_blog_category', array(
        'label'    => esc_html__('Select Category', 'spark-multipurpose'),
        'settings' => 'latest_blog_category',
        'section'  => 'latest_blog_section',
        'choices'  => overclockers_post_category(),
    )));



    $wp_customize->add_setting('latest_blog_page_count', array(
        'transport' => 'postMessage',
        'sanitize_callback' => 'sanitize_text_field',     //done
    ));
    $wp_customize->add_control(new Overclockers_Range_Control($wp_customize, 'latest_blog_page_count', array(
        'label'    => esc_html__('Number of posts', 'spark-multipurpose'),
        'settings' => 'latest_blog_page_count',
        'default' => 6,
        'input_attrs' => array(
            'min' => 3,
            'max' => 100,
            'step' => 1
        ),
        
        'section'  => 'latest_blog_section'
    )));


    $wp_customize->selective_refresh->add_partial('latest_blog_section', array(
        'settings' => array('latest_blog_options','latest_blog_options'),
        'selector' => '.ed-slider',
        'container_inclusive' => true,
        'render_callback' => function() {
            if( in_array( get_theme_mod('latest_blog_options', 'enable') , array( 1, 'enable')) ) {
                return overclockers_banner_section();
            }
        }
    ));