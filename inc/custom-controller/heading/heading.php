<?php
/**
 * Customizer Control: overclockers-checkbox
 *
 * @subpackage  Controls
 * @since       1.0
 */
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( class_exists( 'WP_Customize_Control' ) && ! class_exists( 'Overclockers_Customize_Heading' ) ) :
    /**
     * Custom Heading Control
     */
    class Overclockers_Customize_Heading extends WP_Customize_Control {
        public $type = 'heading';
        /**
		 * enqueue css and scrpts
		 *
		 * @since  1.0.0
		 */
		public function enqueue() {
            wp_enqueue_style('overclockers-heading-control', get_template_directory_uri() . '/inc/custom-controller/heading/heading.css', array(), '1.0.0');
        }
        public function render_content() {
            if (!empty($this->label)) :
                ?>
                <h3 class="overclockers-accordion-section-title"><?php echo esc_html($this->label); ?></h3>
                <?php
            endif;
            if ($this->description) {
                ?>
                <span class="description customize-control-description">
                    <?php echo wp_kses_post($this->description); ?>
                </span>
                <?php
            }
        }
    }
endif;