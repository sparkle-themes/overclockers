<?php
/**
 * Overclockers Theme Customizer
 *
 * @package Overclockers
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function overclockers_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	$wp_customize->register_control_type('Overclockers_Range_Slider_Control');

	$slider_pages = array();

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'overclockers_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'overclockers_customize_partial_blogdescription',
			)
		);
	}


	
	/**
	 * HomePage Settings Panel
	*/
	$wp_customize->add_panel('overclockers_homepage_settings', array(
		'priority' => 5,
		'title' => esc_html__('Home Page Sections', 'overclockers'),
		'description' => esc_html__('Drag and Drop to Reorder', 'overclockers'). '<img class="overclockers-drag-spinner" src="'.admin_url('/images/spinner.gif').'">',
	));

	


	/**
	 * custom controllers
	 */
	require get_template_directory() . '/inc/customizer/slider.php';
	require get_template_directory() . '/inc/customizer/category.php';
	require get_template_directory() . '/inc/customizer/popular-category.php';
	require get_template_directory() . '/inc/customizer/latest-blog.php';
	require get_template_directory() . '/inc/customizer/news-section.php';

	// require get_template_directory() . '/inc/customizer/howto-section.php';
	// require get_template_directory() . '/inc/customizer/gaming-section.php';
	// require get_template_directory() . '/inc/customizer/buyers-section.php';


}
add_action( 'customize_register', 'overclockers_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function overclockers_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function overclockers_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function overclockers_customize_preview_js() {
	wp_enqueue_script( 'overclockers-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), _S_VERSION, true );
}
add_action( 'customize_preview_init', 'overclockers_customize_preview_js' );