<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Overclockers
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function overclockers_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'overclockers_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function overclockers_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'overclockers_pingback_header' );




/**
 * @param string $img - Image url
 * @param string $title - Product title
 * @param string $description - Product description
 * @param string $link - Product url
 * @param string $buttonText - Text on the button that links to the store
 */
function PrintProductBlock( $img, $title, $description, $link, $buttonText, $data = array() ) {
		$price = "";
		if( !empty( $data[10] )){
			$price = '<span class="odd-price">'. $data[7].' € </span> <span class="main-price">'. $data[10].'€ </span>';
		}else{
			$price = '<span class="main-price">'. $data[7].'€ </span>';
		}

		$instock_cls = ' outof-stock';
		if( strtolower( $data[1] ) == 'in stock') 
			$instock_cls = ' in-stock';

	echo '<div class="f-product-wrapper">' .
		'<div class="product-image">' .
			'<div class="tag-list">'.
				'<a href="#">'. $data[2]. '</a>'.
			'</div>'.

			'<img src="' . $img . '" alt="' . $title . '" />' .
		'</div>' .

		'<div class="product-wrapper">' .
		'<h3>' . $title . '</h3>' .
		'<p class="sku">' . $data[0] . '</p>' .
		'<p class="rating">★★★★★ <span>(5)</span></p>' .

		'<p class="pricing">'. $price. '</p>' .

		'<p class="vat-info">(incl. VAT)</p>'.
		'<p class="free-shipping">FREE SHIPPING</p>'.
		'<p class="instock '. $instock_cls. '">'. $data[1]. '</p>'.
		'<a href="' . $link . '" target="_blank" class="product-link btn btn-primary">' . $buttonText . '</a>' .
		'</div>' .
		'<div class="clear"></div>' .
		'</div>';
}



function get_pagination_content() {

	$per_page = get_theme_mod('latest_blog_page_count', 9);
    $selected_category = get_theme_mod('latest_blog_category');
    $cat_ids = array_filter ( explode(',', $selected_category) );

	$current_page = $_POST['page'];
	
	$args = array(
		'posts_per_page' => $per_page,
		'post_type'      => 'post',
		'paged' => $current_page
	);
	if( is_array($cat_ids) && count($cat_ids) > 0){
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'category',
				'field' => 'term_id',
				'terms' => $cat_ids
			)
		);
	}
	

	$latest_blog = new WP_Query ($args);

	ob_start();
	get_template_part( "template-parts/home/latest-blog-loop", '', array('latest_blog' => $latest_blog) );
	$html = ob_get_contents();
	ob_end_clean();
	
	
	ob_start();
	overclockers_pagination($latest_blog, $current_page);
	$pagination = ob_get_contents();
	ob_end_clean();

	$content = array();
	$content['content'] = $html;
	$content['pagination'] = $pagination;
	echo json_encode($content);
    die();
}

add_action( 'wp_ajax_nopriv_get_pagination_content', 'get_pagination_content' );
add_action( 'wp_ajax_get_pagination_content', 'get_pagination_content' );