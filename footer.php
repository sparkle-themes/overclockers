<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Overclockers
 */

?>

</div><!-- #page -->

<div id="rf-wrapper-id"></div>
<?php wp_footer(); ?>

<!-- remote header footer script -->
<script type="text/javascript">

const localRemoteHeaderWrapperHtml = document.getElementById('rh-wrapper-id');

const localRemoteFooterWrapperHtml = document.getElementById('rf-wrapper-id');

const remoteUrl = 'https://www.overclockers.co.uk/remote-component/header';
const localHeadHtml = document.querySelector('head');
let xhr = new XMLHttpRequest();
xhr.responseType = "document";
xhr.open('get', remoteUrl);
xhr.send();
let loadScript = (scriptContent) => {var ss = document.createElement('script'); ss.type = 'text/javascript'; ss.innerHTML = scriptContent; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ss, s); }
let fadeEmbedRemoteHeaderHtml = (primaryHeader) => {primaryHeader?.firstElementChild.setAttribute("style", "height:84px!important; overflow:hidden; opacity:0; transition: opacity 0.15s"); localRemoteHeaderWrapperHtml?.append(primaryHeader); setTimeout( () => {primaryHeader?.firstElementChild.setAttribute("style","opacity:0; transition: opacity 0.15s"); }, 50);
setTimeout( () => {primaryHeader?.firstElementChild.setAttribute("style","opacity:1; transition: opacity 0.15s"); }, 300); localRemoteHeaderWrapperHtml.setAttribute("style", ""); }
xhr.onload = function() {
const rhfCssHtmlList = xhr.response?.querySelectorAll('.remote-header-footer-css');

const rhfJsHtmlList = xhr.response?.querySelector('#remote-header-scripts');

rhfCssHtmlList?.forEach(element => localHeadHtml?.append(element));

const remoteHeaderHtml = xhr.response?.querySelector('header');
const remoteFooterHtml = xhr.response?.querySelector('footer');
const remoteHost = 'https://www.overclockers.co.uk';
	[remoteHeaderHtml,remoteFooterHtml].forEach( html => {
		html.querySelectorAll('a').forEach( links => {
			const relativePath = links.getAttribute('href');
			if(!relativePath.includes('http')){
				links.setAttribute('href',`${remoteHost}${relativePath}`);
			}
		});
	});

    
fadeEmbedRemoteHeaderHtml(remoteHeaderHtml);
localRemoteFooterWrapperHtml?.append(remoteFooterHtml);
loadScript(rhfJsHtmlList.innerHTML);
(async() => {while(!window.hasOwnProperty("initDrillDownMenu")) await new Promise(resolve => setTimeout(resolve, 1000)); window.initDrillDownMenu(); })(); };
</script>
<script id="remote-header-scripts"></script>
<!-- remote header footer script -->

<script>
    jQuery('.mode .btn-toggle').click(function(){
        var t = jQuery(this).toggleClass('active');

        jQuery(this).parent().find('img').removeClass('active');
        if( t.hasClass('active') ) {
            jQuery(this).parent().find('.lite-mode').addClass('active');
            

            let expire = new Date();
            expire.setMonth(expire.getMonth() + 1);

            document.cookie = "mode=dark; expires=" + expire.toUTCString() + "; path=/";
            jQuery('body').addClass('dark');

            
            
        }else{
            jQuery(this).parent().find('.dark-mode').addClass('active');
            
            let expire = new Date();
            expire.setMonth(expire.getMonth() + 1);

            document.cookie = "mode=light; expires=" + expire.toUTCString() + "; path=/";
            jQuery('body').removeClass('dark');
            
        }
        
    });

    jQuery(document).ready(function($){
        setTimeout(() => {
            $('.preloader-wrapper').hide();
        }, 1000 );


        //wpdiscuz guest post author image
        var img = jQuery('.wpd-avatar img');
        if( img.attr('alt') == 'guest') {
            img.attr('src', "https://assets.overclockers.co.uk/assets/ocuk/images/logo.svg" );
            img.attr('srcset', "https://assets.overclockers.co.uk/assets/ocuk/images/logo.svg" );
        }
    })


    //inspiration related post title 
    jQuery('.post-template-fullwidth .crp_related h3').text('Related inspiration posts');

    jQuery('body.home').on('click', '.latest-artical-section .pagination li a', function (e){

        e.preventDefault();

        var link = jQuery(this).attr('href');
        var page = link.match(/\/page\/(\d*)/)[1]; // "35"
        console.log( page );
        jQuery.ajax({
            url: '<?php echo admin_url( 'admin-ajax.php' ) ; ?>',
            type: 'post',
            data: {
                action : 'get_pagination_content',
                page: page,
            },
            beforeSend: function () {
                console.log(' before ');
            },
            success: function (response) {
                response = JSON.parse(response);
                jQuery('.latest-artical-section .latest-artical').append(response.content);
                jQuery('.latest-artical-section .page-navigation').html(response.pagination);

                //replace link 
                jQuery('.latest-artical-section .page-navigation a').each(function(){
                    jQuery(this).attr('href', '<?php echo home_url(); ?>/page/' + jQuery(this).text()  + '/');
                })
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText
                console.log(errorMessage);
            }
        });

    });

</script>
</body>
</html>
