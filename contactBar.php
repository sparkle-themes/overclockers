<div class="contact-bar">
	<div class="container">
		<div class="contact-bar-inner">
			<a href="https://forums.overclockers.co.uk/" target="_blank" rel="noopener noreferrer">OcUK Forum</a>
			<a href="https://help.overclockers.co.uk/hc/en-gb/requests/new" target="_blank" rel="noopener noreferrer">Contact</a>
			<a href="https://www.overclockers.co.uk/" target="_blank" rel="noopener noreferrer">Overclockers Shop</a>
		</div>
	</div>
</div>