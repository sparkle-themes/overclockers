<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Overclockers
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">


	<script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="f0ca96ac-fe5e-41d6-8888-1c23a94322f9" data-blockingmode="auto" type="text/javascript"></script>

	<?php wp_head(); ?>
</head>

<body <?php
$active_mod = "lite";
if( isset( $_COOKIE["mode"] ) && $_COOKIE["mode"] == 'dark' ) {
	body_class( 'dark' );
	$active_mod = "dark";
}else{
	body_class();
}
?>>
<?php wp_body_open(); ?>

<!-- <div class="preloader-wrapper">
    <div class="preloader1">
        <div class="loader loader-inner-1">
            <div class="loader loader-inner-2">
                <div class="loader loader-inner-3">
                </div>
            </div>
        </div>
    </div>
</div> -->

<?php include get_stylesheet_directory() . '/contactBar.php'; ?>

<div id="rh-wrapper-id"></div>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'overclockers' ); ?></a>
	
	<header id="masthead" class="site-header">
		<div class="container">
			<div class="nav-wrapper">
				<nav id="site-navigation" class="main-navigation">
					<button class="menu-toggle" aria-controls="primary-menu3" aria-expanded="false3" data-toggle-target=".header-mobile-menu"  data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".close-nav-toggle">
						<!-- <img src="<?php echo get_template_directory_uri(  );?>/images/blog-menumobile.png"><?php esc_html_e( '', 'overclockers' ); ?> -->
						<i class="material-icon js-material-icon">menu</i> <?php esc_html_e( 'Blog', 'overclockers' ); ?>
					</button>
					
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
						)
					);
					?>
				</nav><!-- #site-navigation -->

				<?php get_search_form(  ); ?>

				<div class="mode">
					<img class="lite-mode <?php if( $active_mod == 'dark') echo 'active'; ?>" src="<?php echo get_template_directory_uri(  );?>/images/lite-mode.png" />
					<img class="dark-mode <?php if( $active_mod !== 'dark') echo 'active'; ?>" src="<?php echo get_template_directory_uri(  );?>/images/sun.png" />
					<button type="button" class="btn btn-toggle <?php if( $active_mod == 'dark') echo 'active'; ?>" data-toggle="button" autocomplete="off">
						<div class="handle"></div>
					</button>
				</div>

			</div>
		</div> <!-- container -->
	</header><!-- #masthead -->
	
	<?php do_action( 'overclockers_add_breadcrumb', 10 ); ?>
