export class CategoryMenu{
	/**
	 * @constructor
	 */
	constructor( $ = jQuery ){
		this.menu = $( '.category-menu p' );
		this.dropDown = $( '#categoryDropdown' );
		this._Compressmenu();
	}

	/**
	 * If the menu does not fit, it will put the overflowing links in a dropdown menu
	 * @private
	 */
	_Compressmenu( $ = jQuery ){
		let overflow = false;
		while( this.menu[0].scrollWidth > this.menu.innerWidth() ){
			overflow = true;
			this._MoveLinkToDropDownMenu();
		}

		if( overflow ){
			this.menu.append( '<span id="categoryMore">More</span>' );

			if( this.menu[0].scrollWidth > this.menu.innerWidth() ){
				this._MoveLinkToDropDownMenu();
			}

			let moreBtn = $( '#categoryMore' );
			let posX = moreBtn.offset().left + moreBtn.width() - this.dropDown.width() - 20;
			// this.dropDown.css( 'left', posX + 'px' );

			moreBtn.click( () =>{
				this.dropDown.toggle();
			} );
		}
	}

	/**
	 * Moves the last child of the menu to the dropdown
	 * @private
	 */
	_MoveLinkToDropDownMenu(){
		let child = this.menu.find( 'a:last-of-type' );
		this.dropDown.append( child );
	}
}