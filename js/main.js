import { CategoryMenu } from "./CategoryMenu.js";
import { loadMore } from "./loadMore.js";
(function ($) {
    $(document).ready(function () {
        new loadMore();

        if ($('.category-menu').length)
            new CategoryMenu();
    });
})(jQuery);