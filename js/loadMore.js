export class loadMore {
    /**
     * @constructor
     */
    constructor($ = jQuery) {
        jQuery('body.home').on('click', '.ajax-pagination .btn', function (e) {
            e.preventDefault();
            var that = jQuery(this);
            that.hide();
            var page = parseInt(jQuery(this).attr('data-page'));
            console.log(page);
            jQuery.ajax({
                url: frontend_ajax_object.ajaxurl,
                type: 'post',
                data: {
                    action: 'get_pagination_content',
                    page: page,
                },
                beforeSend: function () {
                    console.log(' before ');
                },
                success: function (response) {
                    response = JSON.parse(response);
                    if (response.content) {
                        jQuery('.latest-artical-section .latest-artical').append(response.content);
                        that.attr('data-page', page + 1);
                        that.show();
                    } else {
                        that.hide();
                    }
                },
                error: function (xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText
                    console.log(errorMessage);
                }
            });

        });
    }
}