<?php
// Set defaults.
$args = wp_parse_args(
    $args,
    array(
        'class'          => '',
        'cat_link' => false,
        'icon_image' => '',
        'cat_id' => '',
        'cat' => '',
        'first' => array(),
        'second' => array(),
        'hr' => true,
        'title' => '',
    )
);
extract($args);
?>
<div class=" <?php echo esc_attr($class); ?>">

    <?php if( $cat_link ): ?>
        <div class="row">
            
            <div class="col-md-6 news-icon">
                <h1>
                    <?php if($icon_image): ?><img src="<?php echo esc_url($icon_image); ?>"> <?php endif; ?>
                    <?php echo esc_html(get_cat_name($cat_id)); ?>
                </h1>
            </div>
            
            <div class="col-md-6 see-all">
                <a href="<?php echo esc_url(get_category_link($cat_id)); ?>" class="button button1">See All <span>&#62;</span></a>
            </div>
        </div>
    <?php endif; ?>

    
    <?php if( $first ): 
        foreach($first as $post): setup_postdata($post);
        ?>
            <div class="large-artical">
                <div class="col-md-6">
                    <div class="large-img">
                        <a href="<?php echo esc_url(get_the_permalink( )); ?>">
                        <?php the_post_thumbnail( 'large' ); ?>
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="right-artical">
                        <h3 class="blogcolor-news tag-list"><?php overclockers_colorized_category(); ?></h3>
                        <h2><a href="<?php echo esc_url(get_the_permalink( )); ?>"><?php the_title(); ?></a></h2>
                        <?php the_excerpt(  ); ?>
                        <div class="aurther-date">
                            <div class="da-left">
                                <h5><?php overclockers_posted_by( ); ?></h5> <p><span>&#183;</span> </p>
                                <?php overclockers_posted_on(); ?>
                            </div>
                            <h5 class="post-time"><?php echo overclockers_reading_time($post); ?></h5>
                        </div>
                    </div>
                </div>
            </div>
    <?php endforeach; wp_reset_postdata(); endif; ?>

    
    <?php if( $second ): ?>
    <div class="news-blog">
        <?php foreach($second as $post):
            setup_postdata($post); ?>
        <div class="news-block">
            <a href="<?php echo esc_url(get_the_permalink( )); ?>"><?php the_post_thumbnail( 'large' ); ?></a>
            <h3 class="blogcolor-news tag-list">
                <?php overclockers_colorized_category(); ?>
            </h3>
            <h4><a href="<?php echo esc_url(get_the_permalink( )); ?>"><?php the_title(); ?></a></h4>
            <div class="aurther-date">
                <h5><?php overclockers_posted_by(); ?></h5> <p><span>&#183;</span> </p>
                <?php overclockers_posted_on(); ?>
            </div>
            <p><?php echo wp_trim_words( get_the_excerpt( ), 15); ?></p>
        </div>
        <?php endforeach; wp_reset_postdata(); ?>
        
    </div>
    <?php if( $hr ): ?>
    <div class="section-line">
        <hr/>
    </div>
    <?php endif; ?>
    <?php endif; ?>
    
</div>

