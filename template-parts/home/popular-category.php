<?php

$title = get_theme_mod('popular_category_title');
$selected_category = get_theme_mod('pupular_category_category');
$cat_ids = explode(',', $selected_category);


?>

<div class="popular-tags">
    <div class="container">
        <div class="category-header">
            <?php if( $title ): ?>
            <h2><?php echo esc_html($title); ?></h2>
            <?php endif; ?>
            
            <div class="p-tags">
                <?php foreach($cat_ids as $cat_id):
                    $cat = get_category( $cat_id );
                    ?>
                    <h5><a href="<?php echo esc_url(get_category_link($cat_id));?>"><?php echo esc_html($cat->name); ?></a></h5>
                <?php endforeach; ?>
            </div>
        </div>
    </div>  
</div>