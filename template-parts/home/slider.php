<div class="container">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet">
      
        <?php
            $blog = get_theme_mod('slider_category');
            $cat_id = explode(',', $blog);
            $blog_posts = get_theme_mod('slider_count', 6);
            
            $args = array(
                'posts_per_page' => $blog_posts,
                'post_type' => 'post',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'category',
                        'field' => 'term_id',
                        'terms' => $cat_id
                    ),
                ),
            );
            $blog_query = new WP_Query ($args);
        ?>
        <div class="slide-container swiper">
            <div class="slide-content">
                <div class="card-wrapper swiper-wrapper owl-carousel">
                    <?php
                        if ( $blog_query->have_posts() ): while ( $blog_query->have_posts() ) : $blog_query->the_post();
                        ?>
                        <div class="card swiper-slide item">
                            <?php if( has_post_thumbnail()): ?>
                            <div class="image-content">
                                <span class="overlay"></span>
                                
                                <div class="card-image">
                                    <a href="<?php echo esc_url(get_the_permalink( )); ?>"><?php the_post_thumbnail('slider-image'); ?></a>
                                </div>
                            </div>
                            <?php endif; ?>

                            <div class="card-content">
                                <h3 class="blogcolor-how-to tag-list">
                                    <?php overclockers_colorized_category(); ?> 
                                </h3>
                                <h4 class="description"><a href="<?php echo esc_url(get_permalink( )); ?>"><?php the_title(); ?></a></h4>
                                <div class="aurther">
                                    <h5><?php overclockers_posted_by(); ?></h5> 
                                    <p><span>&#183;</span></p>
                                    <h5 class="post-time"><?php echo overclockers_posted_time_ago(); ?></h5>
                                </div>
                            </div>
                        </div>

                        <?php endwhile; endif; ?>
                </div>
            </div>
        </div>
        
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script>
        
        $(document).ready(function(){
            var mainSlider = $('.swiper-wrapper');
            mainSlider.owlCarousel({
                center: false,
                items:4,
                loop:true,
                margin:10,
                stagePadding: 0,
                nav    : true,
                dots: false,
                
                responsive: {
                    0: {
                        items:1,
                    },
                    360: {
                        items:1,
                    },
                    650: {
                        items:2,
                    },
                    850: {
                        items:3,
                    },
                    1024: {
                        items:3,
                    },
                    1400: {
                        items:4,
                    },
                }

            });

            function brandSliderClasses() {
                mainSlider.each(function() {
                    var total = $(this).find('.owl-item.active').length;
                    $(this).find('.owl-item').removeClass('firstactiveitem');
                    $(this).find('.owl-item').removeClass('lastactiveitem');
                    $(this).find('.owl-item.active').each(function(index) {
                        if (index === 0) {
                            $(this).addClass('firstactiveitem');
                        }
                        if (index === total - 1 && total > 1) {
                            $(this).addClass('lastactiveitem');
                            // $(this).css('margin-right', 0);
                        }
                    })
                })
            }
            brandSliderClasses();
            mainSlider.on('translated.owl.carousel', function(event) {
                brandSliderClasses()
            }); 
        });
    </script>
</div>
