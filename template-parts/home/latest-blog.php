<?php
    $title = get_theme_mod('latest_blog_title');
    $per_page = get_theme_mod('latest_blog_page_count', 9);
    $selected_category = get_theme_mod('latest_blog_category');
    $cat_ids = array_filter ( explode(',', $selected_category) );
    
?>
<div class="latest-artical-section">
    <div class="container">
        <?php if( $title ): ?>
        <div class="category-header">
            <h2><?php echo esc_html($title); ?></h2>
        </div>
        <?php endif; ?>

        <div class="latest-artical">
            <?php
            if ($current_page = get_query_var('paged')) {
                $current_page = $current_page;
            } elseif ($current_page = get_query_var('page')) {
                $current_page = $current_page;
            } else {
                $current_page = 1;
            }
            
            $args = array(
                'posts_per_page' => $per_page,
                'post_type'      => 'post',
                'paged' => $current_page
            );
            if( is_array($cat_ids) && count($cat_ids) > 0){
                $args['tax_query'] = array(
                    array(
                        'taxonomy' => 'category',
                        'field' => 'term_id',
                        'terms' => $cat_ids
                    )
                );
            }
            

            $latest_blog = new WP_Query ($args);
            get_template_part( "template-parts/home/latest-blog-loop", '', array('latest_blog' => $latest_blog) );
           ?>

            
        </div>
        <div class="ajax-pagination text-center">
            <button class="btn btn-primary" data-page="2"><?php echo esc_html__( "Load More", 'overclockers' ); ?></button>
        </div>
        <?php //overclockers_pagination($latest_blog); ?>
    </div>
</div>
