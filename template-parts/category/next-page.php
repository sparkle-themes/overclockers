<div class="latest-artical-section list-style">
	<div id="primary" class="site-main container">
		<div class="page-header">
			<?php
			$category = get_queried_object();
			$img = "";
			if ($category instanceof WP_Term) {
				$image_id = get_term_meta ( $category -> term_id, 'category-image-id', true );
				$img = wp_get_attachment_image ( $image_id, 'thumbnail' );
			}
			
			the_archive_title( '<h1 class="page-title news-icon">'.$img, '</h1>' );

			get_template_part( "template-parts/category", 'menu' );
			?>
		</div><!-- .page-header -->
		
		
			
		<?php if ( have_posts() ) : ?>
		<div class="latest-artical-list-view">
			

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'list' );

			endwhile;
			?>
			</div> <!-- latest-artical -->

			<?php

			// the_posts_navigation();
			overclockers_pagination();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
		

	</div><!-- #main -->
	</div>