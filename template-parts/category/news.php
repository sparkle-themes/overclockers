<div class="latest-artical-section">
	<div id="primary" class="site-main container">
		<div class="page-header">
			<?php
			$category = get_queried_object();
			$img = "";
			if ($category instanceof WP_Term) {
				$image_id = get_term_meta ( $category -> term_id, 'category-image-id', true );
				$img = wp_get_attachment_image ( $image_id, 'thumbnail' );
			}
			
			the_archive_title( '<h1 class="page-title news-icon">'.$img, '</h1>' );
			get_template_part( "template-parts/category", 'menu' );
			?>
		</div><!-- .page-header -->
		
		<?php
			$stickyPosts = array();
			$blog_posts = 5;
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

			$icon = $img;
			$args = array(
				'posts_per_page' => $blog_posts,
				'paged' => $paged,
				'post_type' => 'post',
				'tax_query' => array(
					array(
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $category->term_id
					),
				),
			);
			$blog_query = new WP_Query ($args);
			

			$first = array_slice($blog_query->posts,0, 1); // apply array chunk
			$second = array_slice($blog_query->posts, 1, count($blog_query->posts));
			

			get_template_part( "template-parts/home/category", '', array(
				'icon_image' => $icon, 
				'cat_link' => false,
				'cat_id' => $category->term_id, 
				'first' => $first, 
				'second' => $second, 
				
				'hr' =>   true
			));

			if($blog_query->have_posts()) :
				while($blog_query->have_posts()) : $blog_query->the_post();
        			$stickyPosts[] = $post->ID; // add post id to array
				endwhile;
			endif;
				
		?>
		

		<div class="category-header">
            <h2><?php echo esc_html__('Latest Articles', 'overclockers'); ?></h2>
        </div>

		<?php 
		$args = array(
			'post__not_in' => $stickyPosts,
			'paged' => $paged,
			'post_type' => 'post',
			'tax_query' => array(
				array(
					'taxonomy' => 'category',
					'field' => 'term_id',
					'terms' => $category->term_id
				),
			),
		);
		$blog_query = new WP_Query ($args);
		
		if ( $blog_query->have_posts() ) : ?>
			<div class="latest-artical">
				

				<?php
				/* Start the Loop */
				while ( $blog_query->have_posts() ) :
					$blog_query->the_post();

					/*
					* Include the Post-Type-specific template for the content.
					* If you want to override this in a child theme, then include a file
					* called content-___.php (where ___ is the Post Type name) and that will be used instead.
					*/
					get_template_part( 'template-parts/content', 'grid-with-cat' );

				endwhile;
				?>
			</div> <!-- latest-artical -->

			<?php

			// the_posts_navigation();
			overclockers_pagination($blog_query);

		endif;
		?>
		

	</div><!-- #main -->
</div>