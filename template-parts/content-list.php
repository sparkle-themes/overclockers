 <div class="news-block">
    <div class="image-wrapper">
        <a href="<?php echo esc_url(get_the_permalink( )); ?>"><?php the_post_thumbnail('medium'); ?></a>
        <!-- <h3 class="blogcolor-news"><?php //the_category( ' ' ); ?></h3> -->
    </div>
    <div class="content-wrapper">
        <h4><a href="<?php echo esc_url(get_the_permalink( )); ?>"><?php the_title(); ?></a></h4>
        <div class="aurther-date">
            <h5><?php overclockers_posted_by(); ?></h5> <p><span>&#183;</span> </p>
            <?php overclockers_posted_on(); ?>
        </div>
        <p><?php echo wp_trim_words( get_the_content( ), 85); ?></p>
    </div>
</div>