<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Overclockers
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-header">
		<?php overclockers_post_thumbnail(); ?>

		<?php
		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<div class="category-list tag-list">
					<?php overclockers_colorized_category(); ?>
				</div>
				
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<div class="post-on"><?php overclockers_posted_on(); ?></div>
				
				<div class="profile-and-share">
					<?php
						overclockers_posted_by_profile();
						overclockers_share_section();
					?>
				</div>
			</div><!-- .entry-meta -->
		<?php
		
		if ( !is_singular() ) :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
		
		endif; ?>
	</div><!-- .entry-header -->

	

	<div class="entry-content">
		
		<?php
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'overclockers' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			)
		);
		?>

		<div class="content-footer">
			<?php overclockers_share_section(); ?>
			
			<div class="tag-list">
				<?php the_tags('', '', '<br />'); ?>
			</div>
			<div class="profile-and-share footer-content">
				<?php
					
					overclockers_posted_by_profile();
				?>
			</div>
		</div>

		<?php if ( function_exists( 'echo_crp' ) ) { echo_crp(); } ?>
		
		<?php
		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'overclockers' ),
				'after'  => '</div>',
			)
		);
		?>
	</div><!-- .entry-content -->

	<div class="entry-footer">
		<?php overclockers_entry_footer(); ?>
	</div><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
