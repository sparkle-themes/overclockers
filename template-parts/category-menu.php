<?php
if( get_queried_object() ){
    $current_category = get_queried_object();
    $parent_category = $current_category->category_parent;
}else{
    $category = get_the_category();
    $current_category = $category[0];
    $parent_category = $current_category->category_parent;
}

$args = [
    'hide_empty' => 0,
    'parent' => $current_category->cat_ID
];
$category_list = get_categories( $args );
if( $category_list ){
    echo '<div class="category-menu"><p>';
    foreach( $category_list as $cat )
    {
        $class = '';
        if( $cat == $current_category )
            $class = ' class="selected"';

        echo '<a href="' . get_category_link( $cat->cat_ID ) . '"' . $class . '>' . $cat->name . '</a>';
    }
    echo '</p><div id="categoryDropdown"></div>';
    echo '</div>';
}

if( !empty( $current_category->description ) )
    echo '<p class="cat-description">' . $current_category->description . '</p>';