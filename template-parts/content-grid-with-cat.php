<div class="news-block">
    <a href="<?php echo esc_url(get_the_permalink( )); ?>"><?php the_post_thumbnail( 'large' ); ?></a>
    <h3 class="blogcolor-news tag-list">
        <?php overclockers_colorized_category(); ?>
    </h3>
    <h4><a href="<?php echo esc_url(get_the_permalink( )); ?>"><?php the_title(); ?></a></h4>
    <div class="aurther-date">
        <h5><?php overclockers_posted_by(); ?></h5> <p><span>&#183;</span> </p>
        <?php overclockers_posted_on(); ?>
    </div>
    <p><?php echo wp_trim_words( get_the_excerpt( ), 15); ?></p>
</div>